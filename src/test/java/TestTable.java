/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.mmodpoww.programoxoop.programoxoop.Player;
import com.mmodpoww.programoxoop.programoxoop.Table;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Acer
 */
public class TestTable {
    
    public TestTable() {
    }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception {
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
    }

    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testRow1byX(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.setRowCol(0, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
    }
    @Test
    public void testRow2byX(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(1, 0);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
    }
    @Test
    public void testRow3byX(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(2, 0);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
    }
    @Test
    public void testCol1byX(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 0);
        table.setRowCol(1, 0);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
    }
    @Test
    public void testCol2byX(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 1);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
    }
    @Test
    public void testCol3byX(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 2);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
    }
    @Test
    public void testDiagonal1byX(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 0);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
    }
    @Test
    public void testDiagonal2byX(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 2);
        table.setRowCol(1, 1);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(x, table.getWinner());
    }
    @Test
    public void testRowX1byO(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(0, 0);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
    }
    @Test
    public void testRow2byO(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(1, 0);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
    }
    @Test
    public void testRow3byO(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(2, 0);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
    }
    @Test
    public void testCol1byO(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(0, 0);
        table.setRowCol(1, 0);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
    }
    @Test
    public void testCol2byO(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(0, 1);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
    }
    @Test
    public void testCol3byO(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(0, 2);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
    }
    @Test
    public void testDiagonal1byO(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(0, 0);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
    }
    @Test
    public void testDiagonal2byO(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(0, 2);
        table.setRowCol(1, 1);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(o, table.getWinner());
    }
    @Test
    public void testSetRowColIsNotEmptyby1X(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 0);
        assertEquals(false, table.setRowCol(0, 0));
    }
    @Test
    public void testSetRowColIsNotEmptyby2X(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 0);
        table.setRowCol(2, 2);
        assertEquals(false, table.setRowCol(0, 0));
    }
    @Test
    public void testSetRowColIsNotEmptyby3X(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0, 0);
        table.setRowCol(2, 2);
        table.setRowCol(1,1);
        assertEquals(false, table.setRowCol(0, 0));
    }
    @Test
    public void testSetRowColIsNotEmptyby4X(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.setRowCol(0,0);
        table.setRowCol(0,2);
        table.setRowCol(2,0);
        table.setRowCol(2,2);
        assertEquals(false, table.setRowCol(0, 0));
    }
    @Test
    public void testSetRowColIsNotEmptyby1O(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(0, 0);
        assertEquals(false, table.setRowCol(0, 0));
    }
    @Test
    public void testSetRowColIsNotEmptyby2O(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(0, 0);
        table.setRowCol(2, 2);
        assertEquals(false, table.setRowCol(0, 0));
    }
    @Test
    public void testSetRowColIsNotEmptyby3O(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(0, 0);
        table.setRowCol(2, 2);
        table.setRowCol(1,1);
        assertEquals(false, table.setRowCol(0, 0));
    }
    @Test
    public void testSetRowColIsNotEmptyby4O(){
        Player x = new Player('X');
        Player o = new Player('O');
        Table table = new Table(x, o);
        table.switchPlayer();
        table.setRowCol(0,0);
        table.setRowCol(0,2);
        table.setRowCol(2,0);
        table.setRowCol(2,2);
        assertEquals(false, table.setRowCol(0, 0));
    }
    
}
